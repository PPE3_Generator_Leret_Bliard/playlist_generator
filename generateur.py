# -*- coding: utf-8 -*-
"""
Created on Tue Sep 27 15:54:55 2016

@author: martial
"""
import logging
import track

logger = logging.getLogger("generateur")
logger.setLevel(logging.DEBUG)

def main():
    logger.debug("Entrée dans 'main'")
    mon_morceau = track.Track()    
    #INstructions
    logger.debug("Sortie de main")

if __name__ == "__main__":
    logger.debug("Execution du module 'generateur'")
    main()
    logger.debug("Fin du module 'generateur'")
else:
    logger.debug("chargement du module 'generateur'")