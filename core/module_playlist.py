# -*- coding: utf-8 -*-
"""
Created on Tue Sep 27 16:27:16 2016

@author: etudiant
"""

import logging
logger = logging.getLogger("playlist")
logger.setLevel(logging.DEBUG)

class Playlist(object):
    def __init__(self):
        """ constructeur de la classe Playlist """
        logger.debug("Instanciation d'un 'Playlist'")

if __name__ == "__main__":
    logger.debug("execution du module 'playlist'")
    t = Playlist()
    logger.debug("Fin du module 'playlist'")
else:
    logger.debug("chargement du module 'playlist'")