# -*- coding: utf-8 -*-
"""
Created on Tue Sep 27 16:27:16 2016

@author: etudiant
"""
#import artist
#import kind
#import logging
#import album
#import polyphony
#import audioformat

logger = logging.getLogger("track")
logger.setLevel(logging.DEBUG)

class Track(object):
    def __init__(self):
        """ constructeur de la classe Track """
        logger.debug("Instanciation d'un 'Track'")
        self.title = str()
        self.duration = int()
        self.path = str()
        self.kind = kind.Kind()
        self.artist = artist.Artist()
        self.album = album.Album()
        self.polyphony = polyphony.Polyphony()
        self.format = audioformat.AudioFormat()
        
        def getTitle(self):
            """Return track title as a str()
            """
            pass
        
        def setTitle(self, _title):
            """Set track title using the given parameter str()
            """
            old_title = self.title
            self.title = str(_title)
            logger.debug("Track title is now: " + self.title +" (was '" + self.old_title + "')" )

        def getDuration(self):
            """Return track duration as a int()
            """
            pass
        
        def getPath(self):
            """Return track path as a str()
            """
            pass
        
        def getKind(self):
            """Return track title as a str()
            """
            pass

if __name__ == "__main__":
    logger.debug("execution du module 'track'")
    t = Track()
    t.setTitle("new title")
    t.setTitle("my beautiful title")
    logger.debug("Fin du module 'track'")
else:
    logger.debug("chargement du module 'track'")